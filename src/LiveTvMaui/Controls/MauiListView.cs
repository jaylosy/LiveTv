﻿using LiveTvMaui.Models;
using LiveTvMaui.Views;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;

namespace LiveTvMaui.Controls
{
    public class MauiListView : View
    {
        public event EventHandler<EventArgs> Scrolled;
        public event EventHandler<ChannelInfo> ItemSelected;
        
        public static BindableProperty ItemsSourceProperty = BindableProperty.Create(nameof(ItemsSource), typeof(System.Collections.IEnumerable), typeof(MauiListView), Enumerable.Empty<object>());
        
        public System.Collections.IEnumerable ItemsSource
        {
            get => (System.Collections.IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

       
        
        public void SetItemsSource(System.Collections.IEnumerable itemsSource)
        {
            var view = Handler?.PlatformView as MauiListPlatformView;
            view.SetItems(itemsSource?.Cast<object>());
        }
        public void SelectItem(int position)
        {
            var view = Handler?.PlatformView as MauiListPlatformView;
            view.SetSelection(position);
        }

        public void OnScroll()
        {
            Scrolled?.Invoke(this, EventArgs.Empty);
        }
        public void OnItemSelect(ChannelInfo item)
        {
            ItemSelected?.Invoke(this, item);
        }

    }
}
