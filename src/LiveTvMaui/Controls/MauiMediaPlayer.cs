﻿namespace LiveTvMaui.Controls
{
    public class MauiMediaPlayer : View
    {
        public event EventHandler<MediaPlaybackStateChangedEventArgs> MediaPlaybackStateChanged;
        WeakEventManager eventManager = new WeakEventManager();
        internal event EventHandler<EventArgs> RequestPlay
        {
            add => eventManager.AddEventHandler(value);
            remove => eventManager.RemoveEventHandler(value);
        }
        internal event EventHandler<EventArgs> RequestStop
        {
            add => eventManager.AddEventHandler(value);
            remove => eventManager.RemoveEventHandler(value);
        }
        internal event EventHandler<EventArgs> RequestPause
        {
            add => eventManager.AddEventHandler(value);
            remove => eventManager.RemoveEventHandler(value);
        }
        public static BindableProperty SourceProperty = BindableProperty.Create(nameof(Source), typeof(string), typeof(MauiMediaPlayer), "");
        public static BindableProperty VolumeProperty = BindableProperty.Create(nameof(Volume), typeof(float), typeof(MauiMediaPlayer), 1.0f);
        public static BindableProperty AspectProperty = BindableProperty.Create(nameof(Aspect), typeof(Aspect), typeof(MauiMediaPlayer), Aspect.Center);
        public static BindableProperty KernelProperty = BindableProperty.Create(nameof(Kernel), typeof(MediaKernel), typeof(MauiMediaPlayer), MediaKernel.阿里播放器);
        public static BindableProperty CodecProperty = BindableProperty.Create(nameof(Codec), typeof(MediaCodec), typeof(MauiMediaPlayer), MediaCodec.软解);
        public static BindableProperty PlayWhenReadyProperty = BindableProperty.Create(nameof(PlayWhenReady), typeof(bool), typeof(MauiMediaPlayer), false);
        public string Source
        {
            get => GetValue(SourceProperty)?.ToString();
            set => SetValue(SourceProperty, value);
        }
        public MediaKernel Kernel
        {
            get => (MediaKernel)GetValue(KernelProperty);
            set => SetValue(KernelProperty, value);
        }
        public MediaCodec Codec
        {
            get => (MediaCodec)GetValue(KernelProperty);
            set => SetValue(KernelProperty, value);
        }
        public float Volume
        {
            get => (float)GetValue(VolumeProperty);
            set => SetValue(VolumeProperty, value);
        }

        public Aspect Aspect
        {
            get => (Aspect)GetValue(AspectProperty);
            set => SetValue(AspectProperty, value);
        }

        public bool PlayWhenReady
        {
            get => (bool)GetValue(PlayWhenReadyProperty);
            set => SetValue(PlayWhenReadyProperty, value);
        }

        internal void OnMediaPlaybackStateChanged(MediaPlaybackState state)
        {
            MediaPlaybackStateChanged?.Invoke(this, new MediaPlaybackStateChangedEventArgs { State = state });
        }

        public void Play()
        {
            eventManager.HandleEvent(this, EventArgs.Empty, nameof(RequestPlay));
            Handler?.Invoke(nameof(RequestPlay), EventArgs.Empty);
        }

        public void Stop()
        {
            eventManager.HandleEvent(this, EventArgs.Empty, nameof(RequestStop));
            Handler?.Invoke(nameof(RequestStop), EventArgs.Empty);
        }
        public void Pause()
        {
            eventManager.HandleEvent(this, EventArgs.Empty, nameof(RequestPause));
            Handler?.Invoke(nameof(RequestPause), EventArgs.Empty);
        }
    }
}
