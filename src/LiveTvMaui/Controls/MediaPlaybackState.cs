﻿namespace LiveTvMaui.Controls
{
    public enum MediaPlaybackState
    {
        Buffering,
        Playing,
        Paused,
        Stoped,
        Failed
    }
}
