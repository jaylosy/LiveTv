﻿namespace LiveTvMaui.Controls
{
    public class MediaPlaybackStateChangedEventArgs : EventArgs
    {
        public MediaPlaybackState State { get; set; }
    }
}
