﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveTvMaui
{
    public static class GlobalService
    {
        static Dictionary<string,Action> DoAction { get; set; }
        static GlobalService()
        {
            DoAction = new Dictionary<string, Action>();
        }

        public static void ClearAction()
        {
            if (DoAction != null)
            {
                DoAction.Clear();
            }
        }
        public static void RegisterAction(string name,Action action)
        {
            if (DoAction.ContainsKey(name))
            {
                DoAction[name] = action;
            }
            else
            {
                DoAction.Add(name, action);
            }
        }
        public static Action GetAction(string name)
        {
            DoAction.TryGetValue(name, out Action action);
            if (action == null)
            {
                action = () =>
                {
                    Debug.WriteLine($"未实现方法{name}");
                };
            }
            return action;
        }

    }
}
