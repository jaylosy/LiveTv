﻿using CommunityToolkit.Maui.Alerts;
using CommunityToolkit.Maui.Views;
using LiveTvMaui.Controls;
using LiveTvMaui.Models;
#if WINDOWS
using Windows.System;
#endif
namespace LiveTvMaui
{
    public partial class MainPage : ContentPage
    {
        ChannelInfo lastSelectedInfo;
        public MainPage()
        {
            InitializeComponent();
            timer = new System.Timers.Timer();
            timer.Interval = 5000;
            timer.Elapsed += Timer_Elapsed;
            DeviceDisplay.KeepScreenOn= true;
            #region 注册事件
            GlobalService.ClearAction();
            GlobalService.RegisterAction("OnResume", () =>
            {
                var info = context.GetSelectedChannelInfo();
                if (info != null)
                {
                    context.PlayListItemTapCommand.Execute(info);
                }
            });
            GlobalService.RegisterAction("OnPause", mauiMediaPlayer.Stop);
            GlobalService.RegisterAction("KeyDown", () =>
            {
                if (context.SiderViewVisible)
                {
                    if (lastSelectedInfo == null)
                    {
                        lastSelectedInfo = context.GetSelectedChannelInfo();
                    }
                    context.MediaSwipeDown(playList2, false);
                }
                else
                {
                    context.MediaSwipeDownCommand.Execute(playList2);
                }
            });
            GlobalService.RegisterAction("KeyUp", () =>
            {
                if (context.SiderViewVisible)
                {
                    if (lastSelectedInfo == null)
                    {
                        lastSelectedInfo = context.GetSelectedChannelInfo();
                    }
                    context.MediaSwipeUp(playList2, false);
                }
                else
                {
                    context.MediaSwipeUpCommand.Execute(playList2);
                }
            });
            GlobalService.RegisterAction("KeyLeft", () =>
            {
                //context.MediaSwipeUpCommand.Execute(playlist);
            });
            GlobalService.RegisterAction("KeyRight", () =>
            {
                //context.MediaSwipeUpCommand.Execute(playlist);
            });
            GlobalService.RegisterAction("KeyEnter", () =>
            {
                if (!context.SiderViewVisible)
                {
                    context.MediaTapCommand.Execute(null);
                    lastSelectedInfo = null;
                    var info = context.GetSelectedChannelInfo();
                    int index = context.PlayList.IndexOf(info);
                    playList2.SelectItem(index);
                }
                else
                {
                    context.PlayListItemTapCommand.Execute(context.GetSelectedChannelInfo());
                    context.MediaTapCommand.Execute(null);
                }
            });
            GlobalService.RegisterAction("KeyBack", () =>
            {
                if (lastSelectedInfo != null && !context.SiderViewVisible)
                {
                    context.PlayListItemTapCommand.Execute(lastSelectedInfo);
                    lastSelectedInfo = null;
                }
            }); 
            #endregion
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            mauiMediaPlayer_MediaPlaybackStateChanged(null, new MediaPlaybackStateChangedEventArgs
            {
                State = MediaPlaybackState.Failed
            });
        }

        protected override bool OnBackButtonPressed()
        {
            if (context.SiderViewVisible)
            {
                context.MediaTapCommand.Execute(null);
                return true;
            }
            return base.OnBackButtonPressed();
        }

        private void mediaElement_MediaFailed(object sender, CommunityToolkit.Maui.Core.Primitives.MediaFailedEventArgs e)
        {
           
        }

        private void playlist_Scrolled(object sender, EventArgs e)
        {
            if (context.SiderViewVisible)
            {
                context.StartTimer();
            }
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            if (context.SiderViewVisible)
            {
                context.StartTimer();
            }
            var btn = sender as Button;
            if (btn.Text == "上一个")
            {
                GlobalService.GetAction("KeyDown")();
            }
            if (btn.Text == "下一个")
            {
                GlobalService.GetAction("KeyUp")();
            }
            if (btn.Text == "回车")
            {
                GlobalService.GetAction("KeyEnter")();
            }
        }

        private void mauiMediaPlayer_MediaPlaybackStateChanged(object sender, Controls.MediaPlaybackStateChangedEventArgs e)
        {
            
            switch (e.State)
            {
                case Controls.MediaPlaybackState.Failed:
                    timer.Enabled = false;
#if ANDROID
                    try
                    {
                        string url = context.GetNextSource();

                        if (string.IsNullOrEmpty(url))
                        {
                            try
                            {
                                //先暂停
                                GlobalService.GetAction("OnPause")();
                                if (context.MediaKernel == MediaKernel.阿里播放器)
                                {
                                    context.MediaSource = new UriMediaSource { Uri = new Uri("https://gitlab.com/jaylosy/LiveTv/-/raw/main/no-channel.mp4" )};
                                }
                                else
                                {
                                    context.MediaSource = new ResourceMediaSource { Path = "asset:///no-channel.mp4" };

                                }
                            }
                            catch (Exception ex)
                            {
                                context.MediaSwipeUpCommand.Execute(playList2);
                                Toast.Make("播放失败" + ex.Message, CommunityToolkit.Maui.Core.ToastDuration.Short).Show();
                            }
                        }
                        else
                        {
                            //先暂停
                            GlobalService.GetAction("OnPause")();
                            context.MediaSource = new UriMediaSource { Uri = new Uri(url) };
                        }
                    }
                    catch (Exception ex)
                    {
                        Toast.Make("播放失败" + ex.Message, CommunityToolkit.Maui.Core.ToastDuration.Short).Show();
                    }
#else
                    mauiMediaPlayer.Source = "https://gitlab.com/jaylosy/LiveTv/-/raw/main/no-channel.mp4";
                    context.MediaSwipeUpCommand.Execute(playList2);
                    Toast.Make("播放失败", CommunityToolkit.Maui.Core.ToastDuration.Short).Show();
#endif
                    break;
                case Controls.MediaPlaybackState.Buffering:
                    if(timer.Enabled==false)timer.Enabled = true;
                    context.LoadViewVisible = true;
                    break;
                case Controls.MediaPlaybackState.Playing:
                case Controls.MediaPlaybackState.Stoped:
                    timer.Enabled = false;
                    context.LoadViewVisible = false;
                    break;
            }
        }



        System.Timers.Timer timer;

        private void playList2_ItemSelected(object sender, ChannelInfo e)
        {
            context.PlayListItemTapCommand.Execute(e);
        }
    }

}
