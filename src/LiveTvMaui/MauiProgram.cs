﻿using CommunityToolkit.Maui;
using CommunityToolkit.Maui.Core.Handlers;
using LiveTvMaui.Controls;
using LiveTvMaui.ViewHandles;
using Microsoft.Extensions.Logging;

namespace LiveTvMaui
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                // Initialize the .NET MAUI Community Toolkit by adding the below line of code
                .UseMauiCommunityToolkit()
                // Initialize the .NET MAUI Community Toolkit MediaElement by adding the below line of code
                .UseMauiCommunityToolkitMediaElement()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                });

#if DEBUG
            builder.Logging.AddDebug();
#endif

#if ANDROID
            builder.ConfigureMauiHandlers(config =>
            {
                config.AddHandler(typeof(ProgressBar), typeof(ProgressBarViewHandle));
                config.AddHandler(typeof(MauiMediaPlayer), typeof(MauiMediaPlayerViewHandle));
                config.AddHandler(typeof(MauiListView), typeof(MauiListViewHandle));
            });
#endif
            return builder.Build();
        }

    }
}
