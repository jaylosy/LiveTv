﻿using Android.Graphics;
using Android.Media;
using Android.Media.Metrics;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Aliyun.Player;
using Com.Aliyun.Player.Bean;
using Com.Aliyun.Player.Nativeclass;
using Com.Aliyun.Player.Source;
using LiveTvMaui.ViewHandles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveTvMaui.MediaPlugin
{
    public class AliPlayerMediaInterface : BaseMediaInterface, IAliPlayer.IOnStateChangedListener, IAliPlayer.IOnErrorListener, IAliPlayer.IOnLoadingStatusListener, IAliPlayer.IOnInfoListener
    {
        IAliPlayer _player;

        public AliPlayerMediaInterface(MauiMediaPlayerViewHandle handle) : base(handle)
        {
        }

        public override void CreateMediaPlayer()
        {
            if (_player == null)
            {
                _player = AliPlayerFactory.CreateAliPlayer(Context);
                _player.SetTraceId(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                //设置网络超时时间，单位：毫秒
                _player.Config.MNetworkTimeout = 5000;
                //设置超时重试次数。每次重试间隔为networkTimeout。networkRetryCount=0则表示不重试，重试策略app决定，默认值为2
                _player.Config.MNetworkRetryCount = 0;
                
                _player.SetOnStateChangedListener(this);
                _player.SetOnLoadingStatusListener(this);
                _player.SetOnErrorListener(this);
                _player.SetOnInfoListener(this);
            }
            else
            {
                _player.ClearScreen();
                _player.Stop();
                _player.Reset();
            }
            UrlSource source  = new UrlSource();
            source.Uri = _url;
            _player.EnableHardwareDecoder(_mediaCodec == 0 ? false : true);
            _player.SetDataSource(source); 
            _player.Prepare();
            _player.AutoPlay = _autoPlay;
            _player.Volume = _volume;
        }
        public override void SurfaceChanged(ISurfaceHolder holder, [GeneratedEnum] Format format, int width, int height)
        {
            //base.SurfaceChanged(holder, format, width, height);
            _player.SurfaceChanged();
        }
        public override void OnVideoSize(int width, int height)
        {
            switch (_aspect)
            {
                case Aspect.Center:
                case Aspect.AspectFit:
                    _player.SetScaleMode(IPlayer.ScaleMode.ScaleAspectFit);
                    break;
                default:
                    _player.SetScaleMode(IPlayer.ScaleMode.ScaleToFill);
                    break;

            }
        }
        public override void SurfaceCreated(ISurfaceHolder holder)
        {
            try
            {
                //base.SurfaceCreated(holder);
                CreateMediaPlayer();
                _player.SetSurface(holder.Surface);
            }
            catch (Exception ex)
            {

                _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Failed);
                Toast.MakeText(Context, "播放异常！" + ex.Message, ToastLength.Short).Show();
            }
        }
        public override void SurfaceDestroyed(ISurfaceHolder holder)
        {
            //base.SurfaceDestroyed(holder);
            _player.SetSurface(null);
        }
        public void OnError(ErrorInfo p0)
        {
            _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Failed);
            Toast.MakeText(Context, "播放失败！" + p0.Msg, ToastLength.Short).Show();
        }

        public void OnLoadingBegin()
        {
            System.Diagnostics.Debug.WriteLine("loading begin...");
        }

        public void OnLoadingEnd()
        {
            System.Diagnostics.Debug.WriteLine("loading end...");
        }

        public void OnLoadingProgress(int p0, float p1)
        {
            System.Diagnostics.Debug.WriteLine($"loading progress={p0},speed={p1}");
        }

        public override void OnPause()
        {
            if (_player == null) return;
            _player.Pause();
        }

        public override void OnPlay()
        {
            if (_player == null) return;
            _player.Start();
        }

        public override void OnRelease()
        {
            if (_player == null) return;
            _player.Stop();
            _player.Release();
            _player = null;
            GC.Collect();
        }

        public void OnStateChanged(int p0)
        {
            System.Diagnostics.Debug.WriteLine("state=" + p0);
            switch(p0)
            {
                case IAliPlayer.Started:
                    OnVideoSize(_player.VideoWidth, _player.VideoHeight);
                    _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Playing);
                    break;
                case IAliPlayer.Paused:
                    _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Paused);
                    break;
                case IAliPlayer.Stopped:
                case IAliPlayer.Completion:
                    _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Stoped);
                    break;
                case IAliPlayer.Idle:
                    break;
                case IAliPlayer.Error:
                case IAliListPlayer.Unknow:
                    _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Failed);
                    break;
                default:
                    _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Buffering);
                    break;
            }
        }

        public override void OnStop()
        {
            if (_player == null) return;
            _player.Stop();
        }

       
        public override void SetAspect(Aspect aspect)
        {
            base.SetAspect(aspect);
            if (_player == null) return;
            OnVideoSize(_player.VideoWidth, _player.VideoHeight);
        }

        public override void SetAutoPlay(bool autoPlay)
        {
            base.SetAutoPlay(autoPlay);
        }

        public override void SetMediaCodec(int mediaCodec)
        {
            base.SetMediaCodec(mediaCodec);
            if (_player == null) return;
            if (string.IsNullOrEmpty(_url)) return;
            SetUrl(_url);
        }

        public override void SetVolume(float volume)
        {
            base.SetVolume(volume);
            if (_player == null) return;
            _player.Volume = volume;
        }

        public override void SetUrl(string url)
        {
            base.SetUrl(url);
        }

        public void OnInfo(InfoBean p0)
        {
            
        }
    }
}
