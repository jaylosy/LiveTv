﻿using Android.Content;
using Android.Graphics;
using Android.Media;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Com.Google.Android.Exoplayer2.Video;
using LiveTvMaui.Controls;
using LiveTvMaui.ViewHandles;
using Microsoft.Maui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveTvMaui.MediaPlugin
{
    public abstract class BaseMediaInterface : FrameLayout, ISurfaceHolderCallback
    {
        protected SurfaceView _surfaceView;
        static Context context = Platform.CurrentActivity;
        protected Aspect _aspect;
        protected float _volume;
        protected int _mediaCodec;
        protected bool _autoPlay;
        protected string _url;
        protected int _width;
        protected int _height;
        public MediaKernel MediaKernel { get; private set; }
        protected MauiMediaPlayerViewHandle _handle;

        public BaseMediaInterface(MauiMediaPlayerViewHandle handle) : base(context)
        {
            this._handle = handle;
        }
        public virtual void CreateSurfaceView()
        {
            _surfaceView = new SurfaceView(context);
            _surfaceView.Holder.AddCallback(this);
            _surfaceView.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent, GravityFlags.Center);
            AddView(_surfaceView);
        }
        public abstract void CreateMediaPlayer();
        public abstract void OnPlay();
        public abstract void OnPause();
        public abstract void OnStop();
        public abstract void OnRelease();
        public virtual void OnVideoSize(int width, int height)
        {
            this._width = width;
            this._height = height;
            var dm = Platform.CurrentActivity.Resources.DisplayMetrics;
           

            if (_aspect == Aspect.Center || _aspect == Aspect.AspectFit)
            {
                var max = Math.Max((float)width / (float)dm.WidthPixels, (float)height / (float)dm.HeightPixels);

                int realWidth = Convert.ToInt32(width / max);
                int realHeight = Convert.ToInt32(height /max);

                _surfaceView.LayoutParameters = new FrameLayout.LayoutParams(realWidth, realHeight, GravityFlags.Center);
                //_surfaceView.ScaleX = 0.95f;
                //_surfaceView.ScaleY = 0.95f;
                return;
            }
            else
            {
                _surfaceView.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent, GravityFlags.Center);
                //_surfaceView.ScaleX = 1.0f;
                //_surfaceView.ScaleY = 1.0f;
                return;
            }

        }
        public virtual void SetVolume(float volume)
        {
            this._volume = volume;
        }
        public virtual void SetAspect(Aspect aspect)
        {
            this._aspect = aspect;
        }
        public virtual void SetAutoPlay(bool autoPlay)
        {

            this._autoPlay = autoPlay;
        }
        public void SetKernel(MediaKernel mediaKernel)
        {

            this.MediaKernel = mediaKernel;
        }
        public virtual void SetUrl(string url)
        {
            try
            {
                if (string.IsNullOrEmpty(url)) return;
                this._url = url;
                if (_surfaceView == null)
                {
                    CreateSurfaceView();
                }
                else
                {
                    CreateMediaPlayer();
                }
            }
            catch (Exception ex)
            {
                _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Failed);
                Toast.MakeText(Context, "播放异常！" + ex.Message, ToastLength.Short).Show();
            }
        }
        /// <summary>
        /// 设置解码方式
        /// </summary>
        /// <param name="mediaCodec">0.软解，1.硬解</param>
        public virtual void SetMediaCodec(int mediaCodec)
        {
            this._mediaCodec = mediaCodec;
        }

        public virtual void SurfaceChanged(ISurfaceHolder holder, [GeneratedEnum] Format format, int width, int height)
        {
            
        }

        public virtual void SurfaceCreated(ISurfaceHolder holder)
        {
            try
            {
                CreateMediaPlayer();
            }
            catch (Exception ex)
            {
                _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Failed);
                Toast.MakeText(Context, "播放异常！" + ex.Message, ToastLength.Short).Show();
            }
        }

        public virtual void SurfaceDestroyed(ISurfaceHolder holder)
        {
            
        }
    }
}
