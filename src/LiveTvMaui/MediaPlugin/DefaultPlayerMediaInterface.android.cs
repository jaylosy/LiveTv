﻿using Android.Media;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Google.Android.Exoplayer2;
using Com.Google.Android.Exoplayer2.Analytics;
using LiveTvMaui.ViewHandles;

namespace LiveTvMaui.MediaPlugin
{
    public class DefaultPlayerMediaInterface : BaseMediaInterface
    {
        VideoView _player;
        MediaPlayer _mp;

        public DefaultPlayerMediaInterface(MauiMediaPlayerViewHandle handle) : base(handle)
        {
        }

        public override void CreateSurfaceView() 
        {
            _player = new VideoView(Context);
            _player.Prepared += _player_Prepared;
            _player.Error += _player_Error;
            _player.Info += _player_Info;
            _player.Completion += _player_Completion;
            MediaController controller = new MediaController(Context);
            controller.Visibility = ViewStates.Invisible;
            _player.SetMediaController(controller);
            _player.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent, GravityFlags.Center);
            AddView(_player);
            CreateMediaPlayer();
        }
        public override void OnVideoSize(int width, int height)
        {
            this._width = width;
            this._height = height;
            var dm = Platform.CurrentActivity.Resources.DisplayMetrics;


            if (_aspect == Aspect.Center || _aspect == Aspect.AspectFit)
            {
                var max = Math.Max((float)width / (float)dm.WidthPixels, (float)height / (float)dm.HeightPixels);

                int realWidth = Convert.ToInt32(width / max);
                int realHeight = Convert.ToInt32(height / max);

                _player.LayoutParameters = new FrameLayout.LayoutParams(realWidth, realHeight, GravityFlags.Center);
                return;
            }
            else
            {
                _player.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent, GravityFlags.Center);
                return;
            }
        }
        private void _player_Completion(object sender, EventArgs e)
        {
            _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Stoped);
        }

        private void _player_Info(object sender, MediaPlayer.InfoEventArgs e)
        {
            _mp = e.Mp;
            switch (e.What)
            {
                case MediaInfo.BufferingStart:
                    _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Buffering);
                    break;
                case MediaInfo.BufferingEnd:
                    if (e.Mp.IsPlaying)
                    {
                        OnVideoSize(_player.Width, _player.Height);
                        _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Playing);
                        _mp.SetVolume(_volume, _volume);
                    }
                    break;
            }
        }

        private void _player_Error(object sender, MediaPlayer.ErrorEventArgs e)
        {
            _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Failed);
        }

        private void _player_Prepared(object sender, EventArgs e)
        {
            if (_autoPlay)
            {
                OnPlay();
                _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Playing);
            }
        }

        public override void CreateMediaPlayer()
        {
            OnStop();
            _player.SetVideoURI(Android.Net.Uri.Parse(_url));
        }



        public override void OnPause()
        {
            if (_player == null) return;
            _player.Pause();
        }

        public override void OnPlay()
        {
            if (_player == null) return;
            _player.Start();
        }

        public override void OnRelease()
        {
            if (_player == null) return;
            _player.StopPlayback();
            _player.Dispose();
            _player = null;
        }


        public override void OnStop()
        {
            if (_player == null) return;
            _player.StopPlayback();
        }



        public override void SetAspect(Aspect aspect)
        {
            base.SetAspect(aspect);
            if (_player == null) return;
            if (!_player.IsPlaying) return;
            OnVideoSize(_player.Width, _player.Height);
        }

        public override void SetAutoPlay(bool autoPlay)
        {
            base.SetAutoPlay(autoPlay);
        }

        public override void SetMediaCodec(int mediaCodec)
        {
            base.SetMediaCodec(mediaCodec);
        }

        public override void SetVolume(float volume)
        {
            base.SetVolume(volume);
            if (_player == null) return;
            if (_mp == null) return;
            _mp.SetVolume(volume, volume);
        }
    }
}
