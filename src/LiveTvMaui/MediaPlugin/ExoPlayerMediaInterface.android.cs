﻿using Android.Appwidget;
using Android.Media;
using Android.Media.Metrics;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Google.Android.Exoplayer2;
using Com.Google.Android.Exoplayer2.Analytics;
using Com.Google.Android.Exoplayer2.Metadata;
using Com.Google.Android.Exoplayer2.Text;
using Com.Google.Android.Exoplayer2.Trackselection;
using Com.Google.Android.Exoplayer2.UI;
using Com.Google.Android.Exoplayer2.Video;
using LiveTvMaui.ViewHandles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LiveTvMaui.MediaPlugin
{
    public class ExoPlayerMediaInterface : BaseMediaInterface,IPlayer.IListener
    {
        StyledPlayerView _player;

        public ExoPlayerMediaInterface(MauiMediaPlayerViewHandle handle) : base(handle)
        {
        }
        
        public override void CreateSurfaceView()
        {
            _player = new StyledPlayerView(Context);
            _player.UseController = false;
            _player.Player = new IExoPlayer.Builder(Context).Build();
            _player.Player.AddListener(this);
            _player.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent);
            this.AddView(_player);
            CreateMediaPlayer();
        }
        public override void CreateMediaPlayer()
        {
            OnStop();
            var item = MediaItem.FromUri(Android.Net.Uri.Parse(_url));
            _player.Player.SetMediaItem(item);
            _player.Player.Prepare();
            _player.Player.PlayWhenReady = _autoPlay;
            _player.Player.Volume = _volume;
        }

        public void OnAudioAttributesChanged(Com.Google.Android.Exoplayer2.Audio.AudioAttributes audioAttributes)
        {
            
        }

        public void OnAudioSessionIdChanged(int audioSessionId)
        {
            
        }

        public void OnAvailableCommandsChanged(IPlayer.Commands availableCommands)
        {
            
        }

        public void OnCues(CueGroup cueGroup)
        {
            
        }

        public void OnDeviceInfoChanged(Com.Google.Android.Exoplayer2.DeviceInfo deviceInfo)
        {
            
        }

        public void OnDeviceVolumeChanged(int volume, bool muted)
        {
            
        }

        public void OnEvents(IPlayer player, IPlayer.Events events)
        {
           
        }

        public void OnIsLoadingChanged(bool isLoading)
        {
            
        }

        public void OnIsPlayingChanged(bool isPlaying)
        {
            
        }

        public void OnLoadingChanged(bool isLoading)
        {
            
        }

        public void OnMaxSeekToPreviousPositionChanged(long maxSeekToPreviousPositionMs)
        {
            
        }

        public void OnMediaItemTransition(MediaItem mediaItem, int reason)
        {
            
        }

        public void OnMediaMetadataChanged(Com.Google.Android.Exoplayer2.MediaMetadata mediaMetadata)
        {
            
        }

        public void OnMetadata(Metadata metadata)
        {
            
        }

        public override void OnPause()
        {
            if (_player == null) return;
            _player.Player.Pause();
        }

        public override void OnPlay()
        {
            if (_player == null) return;
            _player.Player.Play();
        }

        public void OnPlaybackParametersChanged(PlaybackParameters playbackParameters)
        {
            
        }

        public void OnPlaybackStateChanged(int playbackState)
        {
            
        }

        public void OnPlaybackSuppressionReasonChanged(int playbackSuppressionReason)
        {
            
        }

        public void OnPlayerError(PlaybackException error)
        {
            
        }

        public void OnPlayerErrorChanged(PlaybackException error)
        {
            _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Failed);
            Toast.MakeText(Context, "播放失败！"+error.Message, ToastLength.Short).Show();
        }

        public void OnPlayerStateChanged(bool playWhenReady, int playbackState)
        {
            
            Log.Debug("state", playbackState.ToString());
            switch (playbackState)
            {
                case PlaybackStats.PlaybackStatePlaying:
                    OnVideoSize(_player.Player.VideoSize.Width, _player.Player.VideoSize.Height);
                    _handle.VirtualView.OnMediaPlaybackStateChanged( Controls.MediaPlaybackState.Playing);
                    break;
                case PlaybackStats.PlaybackStateFailed:
                    _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Failed);
                    break;
                case PlaybackStats.PlaybackStateBuffering:
                case PlaybackStats.PlaybackStateJoiningForeground:
                case PlaybackStats.PlaybackStateJoiningBackground:
                    _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Buffering);
                    break;
                case PlaybackStats.PlaybackStatePaused:
                    _handle.VirtualView.OnMediaPlaybackStateChanged(Controls.MediaPlaybackState.Paused);
                    break;
            }
        }
        public void OnPlaylistMetadataChanged(Com.Google.Android.Exoplayer2.MediaMetadata mediaMetadata)
        {
            
        }

        public void OnPlayWhenReadyChanged(bool playWhenReady, int reason)
        {
            
        }

        public void OnPositionDiscontinuity(int reason)
        {
            
        }

        public override void OnRelease()
        {
            if (_player == null) return;
            _player.Player.Stop();
            _player.Player.Release();
            _player.Player = null;
            _player.Dispose();
            _player = null;
        }

        public void OnRenderedFirstFrame()
        {
            
        }

        public void OnRepeatModeChanged(int repeatMode)
        {
            
        }

        public void OnSeekBackIncrementChanged(long seekBackIncrementMs)
        {
            
        }

        public void OnSeekForwardIncrementChanged(long seekForwardIncrementMs)
        {
            
        }

        public void OnSeekProcessed()
        {
            
        }

        public void OnShuffleModeEnabledChanged(bool shuffleModeEnabled)
        {
            
        }

        public void OnSkipSilenceEnabledChanged(bool skipSilenceEnabled)
        {
            
        }

        public override void OnStop()
        {
            if (_player == null) return;
            _player.Player.Stop();
        }

        public void OnSurfaceSizeChanged(int width, int height)
        {
            
        }

        public void OnTimelineChanged(Timeline timeline, int reason)
        {
            
        }

        public void OnTracksChanged(Tracks tracks)
        {
            
        }

        public void OnTrackSelectionParametersChanged(TrackSelectionParameters parameters)
        {
            
        }

        public void OnVideoSizeChanged(VideoSize videoSize)
        {
            
        }

        public void OnVolumeChanged(float volume)
        {
            
        }

        public override void SetAspect(Aspect aspect)
        {
            base.SetAspect(aspect);
            if (_player == null) return;
            if (!_player.Player.IsPlaying) return;
            OnVideoSize(_player.Player.VideoSize.Width, _player.Player.VideoSize.Height);
        }
        public override void OnVideoSize(int width, int height)
        {
            switch (_aspect)
            {
                case Aspect.AspectFit:
                case Aspect.Center:
                    _player.ResizeMode = AspectRatioFrameLayout.ResizeModeFit; break;
                case Aspect.Fill:
                    _player.ResizeMode = AspectRatioFrameLayout.ResizeModeFill; break;
                case Aspect.AspectFill:
                    _player.ResizeMode = AspectRatioFrameLayout.ResizeModeFixedWidth; break;
            }
        }

        public override void SetAutoPlay(bool autoPlay)
        {
            base.SetAutoPlay(autoPlay);
        }

        public override void SetMediaCodec(int mediaCodec)
        {
            base.SetMediaCodec(mediaCodec);
        }

        public override void SetVolume(float volume)
        {
            base.SetVolume(volume);
            if (_player == null) return;
            _player.Player.Volume = volume;
        }
    }
}
