﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveTvMaui.Models
{
	public class ChannelInfo : BaseModel
	{
		private string _title;

		public string Title
		{
			get { return _title; }
			set
			{
				_title = value;
				OnPropertyChanged(nameof(Title));
			}
		}

		public ObservableCollection<string> Url { get; set; }
		private Color _textColor = Color.FromArgb("#FFF");

		public Color TextColor
		{
			get { return _textColor; }
			set
			{
				_textColor = value;
				OnPropertyChanged(nameof(TextColor));
			}
		}

		private bool _selected;

		public bool Selected
		{
			get { return _selected; }
			set
			{
				_selected = value;
				if(_selected)
				{
					TextColor = Color.FromArgb("#1E90FF");
				}
				else
				{
                    TextColor = Color.FromArgb("#FFF");
                }
				OnPropertyChanged(nameof(Selected));
			}
		}

	}
}
