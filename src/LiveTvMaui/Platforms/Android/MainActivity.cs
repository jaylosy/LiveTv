﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;

namespace LiveTvMaui
{
    [Activity(Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize | ConfigChanges.Density,ScreenOrientation = ScreenOrientation.SensorLandscape)]
    public class MainActivity : MauiAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            //全屏，即隐藏状态栏，时间、信号这些也不可见
            Window.SetFlags(Android.Views.WindowManagerFlags.Fullscreen, Android.Views.WindowManagerFlags.Fullscreen);

            //半透明 任务栏
            // Window.SetFlags(Android.Views.WindowManagerFlags.TranslucentStatus, Android.Views.WindowManagerFlags.TranslucentStatus);

            //全透明任务栏
            //Window.SetFlags(Android.Views.WindowManagerFlags.TranslucentNavigation, Android.Views.WindowManagerFlags.TranslucentNavigation);


            //设置状态栏、导航栏色颜色为透明
            //Window.SetStatusBarColor(Android.Graphics.Color.Transparent);
            //Window.SetNavigationBarColor(Android.Graphics.Color.Transparent);
            base.OnCreate(savedInstanceState);
        }
        protected override void OnResume()
        {
            base.OnResume();
            GlobalService.GetAction(nameof(OnResume))();
        }
        protected override void OnPause()
        {
            base.OnPause();
            GlobalService.GetAction(nameof(OnPause))();
        }
        public override bool OnKeyDown([GeneratedEnum] Keycode keyCode, KeyEvent e)
        {
            switch (keyCode)
            {
                case Keycode.DpadUp:
                case Keycode.ChannelUp:
                    GlobalService.GetAction("KeyUp")();
                    break;
                case Keycode.DpadDown:
                case Keycode.ChannelDown:
                    if (e.Action == KeyEventActions.Down)
                    {
                        GlobalService.GetAction("KeyDown")();
                    }
                    break;
                case Keycode.DpadCenter:
                case Keycode.Enter:
                    GlobalService.GetAction("KeyEnter")();
                    break;
                case Keycode.DpadLeft:
                    GlobalService.GetAction("KeyLeft")();
                    break;
                case Keycode.DpadRight:
                    GlobalService.GetAction("KeyRight")();
                    break;
                case Keycode.Back:
                    GlobalService.GetAction("KeyBack")();
                    break;
            }
            return base.OnKeyDown(keyCode, e);
        }
    }
}
