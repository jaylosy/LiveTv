﻿using Android.Content;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveTvMaui
{
    public class ProgressBarView: Android.Widget.ProgressBar
    {

        public ProgressBarView() : base(Platform.CurrentActivity)
        {
            this.Indeterminate = true;
        }
    }
}
