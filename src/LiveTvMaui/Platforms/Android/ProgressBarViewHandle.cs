﻿using Microsoft.Maui;
using Microsoft.Maui.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveTvMaui
{
    internal class ProgressBarViewHandle : ViewHandler<ProgressBar, ProgressBarView>
    {
        static IPropertyMapper mapper = new PropertyMapper<ProgressBar, ProgressBarViewHandle>();
        static CommandMapper commandMapper = new CommandMapper<ProgressBar, ProgressBarViewHandle>();
        public ProgressBarViewHandle() : base(mapper, commandMapper)
        {
        }

        protected override ProgressBarView CreatePlatformView()
        {
            return new ProgressBarView();
        }
    }
}
