﻿using Android.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveTvMaui.Services
{
    internal class PlatformService
    {
        public static void ShowSingleChioce(string title,string[] items,int selectedIndex,Action<int> selectedIndexAction)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(Platform.CurrentActivity)
                .SetTitle(title)
                .SetSingleChoiceItems(items, selectedIndex, (s, e) =>
                {
                    selectedIndexAction(e.Which);
                    (s as AlertDialog).Dismiss();
                });
            builder.Create().Show();
        }
    }
}
