﻿using LiveTvMaui.Controls;
using LiveTvMaui.Views;
using Microsoft.Maui.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveTvMaui.ViewHandles
{
    public partial class MauiListViewHandle : ViewHandler<MauiListView, MauiListPlatformView>
    {
        protected override MauiListPlatformView CreatePlatformView()
        {
            var view = new MauiListPlatformView(this);
            //view.ta
            return view;
        }


    }
}
