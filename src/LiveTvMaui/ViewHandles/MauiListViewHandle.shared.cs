﻿using LiveTvMaui.Controls;
using LiveTvMaui.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveTvMaui.ViewHandles
{
    public partial class MauiListViewHandle
    {
        static IPropertyMapper mapper = new PropertyMapper<MauiListView, MauiListViewHandle>
        {
            [nameof(MauiListView.ItemsSource)] = MapItemsSource,
        };

        private static void MapItemsSource(MauiListViewHandle handle, MauiListView view)
        {
            var changed = view.ItemsSource as INotifyCollectionChanged;
            if (changed != null)
            {
                changed.CollectionChanged += (s, e) =>
                {
                    handle?.PlatformView.SetItems(view.ItemsSource?.Cast<object>());
                };
            }
            handle?.PlatformView.SetItems(view.ItemsSource?.Cast<object>());
        }

        static CommandMapper commandMapper = new CommandMapper<MauiListView, MauiListViewHandle>();
        public MauiListViewHandle() : base(mapper, commandMapper)
        {
        }
    }
}
