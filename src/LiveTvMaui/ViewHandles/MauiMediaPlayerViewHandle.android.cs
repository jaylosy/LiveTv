﻿using LiveTvMaui.Controls;
using LiveTvMaui.Views;
using Microsoft.Maui.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveTvMaui.ViewHandles
{
    public partial class MauiMediaPlayerViewHandle : ViewHandler<MauiMediaPlayer, MauiMediaPlayerView>
    {

        protected override MauiMediaPlayerView CreatePlatformView()
        {
            return new MauiMediaPlayerView(this);
        }
    }
}
