﻿using LiveTvMaui.Controls;

namespace LiveTvMaui.ViewHandles
{
    public partial class MauiMediaPlayerViewHandle
    {
        static IPropertyMapper mapper = new PropertyMapper<MauiMediaPlayer, MauiMediaPlayerViewHandle>
        {
            [nameof(MauiMediaPlayer.Kernel)] = MapKernel,
            [nameof(MauiMediaPlayer.Codec)] = MapCodec,
            [nameof(MauiMediaPlayer.PlayWhenReady)] = MapPlayWhenReady,
            [nameof(MauiMediaPlayer.Aspect)] = MapAspect,
            [nameof(MauiMediaPlayer.Volume)] = MapVolume,
            [nameof(MauiMediaPlayer.Source)] = MapSource,
        };

        private static void MapCodec(MauiMediaPlayerViewHandle handle, MauiMediaPlayer player)
        {
            handle.PlatformView.SetCodec(player.Codec);
        }

        private static void MapKernel(MauiMediaPlayerViewHandle handle, MauiMediaPlayer player)
        {
            handle.PlatformView.SetKernel(player.Kernel);
        }

        static CommandMapper commandMapper = new CommandMapper<MauiMediaPlayer, MauiMediaPlayerViewHandle>
        {
            [nameof(MauiMediaPlayer.RequestPlay)] = MapRequestPlay,
            [nameof(MauiMediaPlayer.RequestPause)] = MapRequestPause,
            [nameof(MauiMediaPlayer.RequestStop)] = MapRequestStop,
        };

        private static void MapRequestStop(MauiMediaPlayerViewHandle handle, MauiMediaPlayer player, object arg3)
        {
            handle.PlatformView.RequestStop();
        }

        private static void MapRequestPause(MauiMediaPlayerViewHandle handle, MauiMediaPlayer player, object arg3)
        {
            handle.PlatformView.RequestPause();
        }

        private static void MapRequestPlay(MauiMediaPlayerViewHandle handle, MauiMediaPlayer player, object arg3)
        {
            handle.PlatformView.RequestPlay();
        }

        private static void MapVolume(MauiMediaPlayerViewHandle handle, MauiMediaPlayer player)
        {
            handle.PlatformView.SetVolume(player.Volume);
        }

        private static void MapAspect(MauiMediaPlayerViewHandle handle, MauiMediaPlayer player)
        {
            handle.PlatformView.SetAspect(player.Aspect);
        }

        private static void MapSource(MauiMediaPlayerViewHandle handle, MauiMediaPlayer player)
        {
            handle.PlatformView.SetUrl(player.Source);
        }

        private static void MapPlayWhenReady(MauiMediaPlayerViewHandle handle, MauiMediaPlayer player)
        {
            handle.PlatformView.SetAutoPlay(player.PlayWhenReady);
        }

        public MauiMediaPlayerViewHandle() : base(mapper, commandMapper)
        {
        }

    }
}
