﻿using CommunityToolkit.Maui.Alerts;
using CommunityToolkit.Maui.Views;
using LiveTvMaui.Controls;
using LiveTvMaui.Models;
using LiveTvMaui.Services;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;

namespace LiveTvMaui.ViewModels
{
    public class MainPageViewModel : BaseModel
    {
        DateTime lastUpdateDate = DateTime.Now;
        private bool _infoViewVisible;

        public bool InfoViewVisible
        {
            get { return _infoViewVisible; }
            set
            {
                _infoViewVisible = value;
                OnPropertyChanged(nameof(InfoViewVisible));
            }
        }

        private float _volume;

        public float Volume
        {
            get { return _volume; ; }
            set
            {
                if (SiderViewVisible)
                {
                    StartTimer();
                }
                _volume = value;
                OnPropertyChanged(nameof(Volume));
            }
        }
        private MediaKernel _mediaKernel;

        public MediaKernel MediaKernel
        {
            get { return _mediaKernel; }
            set
            {
                _mediaKernel = value;
                OnPropertyChanged(nameof(MediaKernel));
            }
        }

        private bool _loadViewVisible;

        public bool LoadViewVisible
        {
            get { return _loadViewVisible; }
            set
            {
                _loadViewVisible = value;
                OnPropertyChanged(nameof(LoadViewVisible));
            }
        }

        private bool _siderViewVisible;

        public bool SiderViewVisible
        {
            get { return _siderViewVisible; }
            set
            {
                if (value && InfoViewVisible)
                {
                    InfoViewVisible = false;
                }
                _siderViewVisible = value;
                OnPropertyChanged(nameof(SiderViewVisible));
            }
        }
        private MediaSource _mediaSource;

        public MediaSource MediaSource
        {
            get { return _mediaSource; }
            set
            {
                _mediaSource = value;
                OnPropertyChanged(nameof(MediaSource));
            }
        }

        private Aspect _mediaAspect = Aspect.Center;

        public Aspect MediaAspect
        {
            get { return _mediaAspect; }
            set
            {
                if (SiderViewVisible)
                {
                    StartTimer();
                }
                _mediaAspect = value;
                OnPropertyChanged(nameof(MediaAspect));
            }
        }

        public ObservableCollection<ChannelInfo> PlayList { get; set; }

        private string _title;

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged(nameof(Title));
            }
        }
        private string _currTime;

        public string CurrTime
        {
            get { return _currTime; }
            set
            {
                _currTime = value;
                OnPropertyChanged(nameof(CurrTime));
            }
        }
        private MediaCodec _mediaCodec;

        public MediaCodec MediaCodec
        {
            get { return _mediaCodec; }
            set
            {
                _mediaCodec = value;
                OnPropertyChanged(nameof(MediaCodec));
            }
        }

        public Command MediaTapCommand { get; set; }
        public Command MediaSwipeUpCommand { get; set; }
        public Command MediaSwipeDownCommand { get; set; }
        public Command PlayListItemTapCommand { get; set; }
        public Command SetMediaKernelCommand { get; set; }
        public Command SetMediaSizeCommand { get; set; }
        public Command SetSourceCommand { get; set; }
        public Command SetMediaCodecCommand { get; set; }
        System.Timers.Timer timer;
        public MainPageViewModel()
        {
            MediaKernel = MediaKernel.谷歌播放器;
            MediaCodec = MediaCodec.软解;
            Volume = 1;
            MediaAspect = Aspect.Center;
            LoadViewVisible = true;
            PlayList = new ObservableCollection<ChannelInfo>();
            MediaTapCommand = new Command(OnMediaTap);
            MediaSwipeUpCommand = new Command(OnMediaSwipeUp);
            MediaSwipeDownCommand = new Command(OnMediaSwipeDown);
            PlayListItemTapCommand = new Command(OnPlayListItemTap);
            SetMediaSizeCommand = new Command(OnSetMediaSize);
            SetSourceCommand = new Command(OnSetSourceCommand);
            SetMediaKernelCommand = new Command(OnSetMediaKernel);
            SetMediaCodecCommand = new Command(OnSetMediaCodec);
            InitCurrTime();
            LoadPlayList();

            timer = new System.Timers.Timer();
            timer.Interval = 5000;
            timer.Elapsed += Timer_Elapsed;
        }

        private void OnSetSourceCommand(object obj)
        {
            var info = GetSelectedChannelInfo();
            if (info == null) return;
            int index = 0;
            if (MediaSource is UriMediaSource)
            {
                string url = (MediaSource as UriMediaSource).Uri.ToString();
                index = info.Url.IndexOf(url);
            }
            else
            {
                return;
            }
            PlatformService.ShowSingleChioce("播放源", info.Url.ToArray(), index, (selectedIndex) =>
            {
                //先暂停
                GlobalService.GetAction("OnPause")();
                try
                {
                    MediaSource = new UriMediaSource
                    {
                        Uri = new Uri(info.Url[selectedIndex])
                    };
                }
                catch (Exception ex)
                {
                    Toast.Make(ex.Message, CommunityToolkit.Maui.Core.ToastDuration.Short).Show();
                }
            });
        }

        private void OnSetMediaCodec(object obj)
        {
            var btn = obj as Button;
            if (btn.Text == MediaCodec.软解.ToString())
            {
                btn.Text = MediaCodec.硬解.ToString();
                MediaCodec = MediaCodec.软解;
            }
            else
            {
                btn.Text = MediaCodec.软解.ToString();
                MediaCodec = MediaCodec.硬解;
            }
        }

        private void OnSetMediaKernel(object obj)
        {
            var btn = obj as Button;
            if (btn.Text == MediaKernel.内置播放器.ToString())
            {
                btn.Text = MediaKernel.阿里播放器.ToString();
                MediaKernel = MediaKernel.内置播放器;
            }
            else if (btn.Text == MediaKernel.阿里播放器.ToString())
            {
                btn.Text = MediaKernel.谷歌播放器.ToString();
                MediaKernel = MediaKernel.阿里播放器;
            }
            else
            {
                btn.Text = MediaKernel.内置播放器.ToString();
                MediaKernel = MediaKernel.谷歌播放器;
            }
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            StopTimer();
            if (SiderViewVisible)
            {
                OnMediaTap();
            }
            if (InfoViewVisible)
            {
                InfoViewVisible = !InfoViewVisible;
            }
        }

        public void StartTimer()
        {
            timer.Enabled = false;
            timer.Enabled = true;
        }
        public void StopTimer()
        {
            timer.Enabled = false;
        }
        private void OnSetMediaSize(object obj)
        {
            var btn = obj as Button;
            if (btn.Text == "拉伸")
            {
                btn.Text = "原始";
                MediaAspect = Aspect.Fill;
            }
            else
            {
                btn.Text = "拉伸";
                MediaAspect = Aspect.Center;
            }
        }

        Task InitCurrTime()
        {
            return Task.Run(() =>
            {
                while (true)
                {
                    if (DateTime.Now > lastUpdateDate.AddMinutes(10))
                    {
                        LoadPlayList(false);
                    }
                    CurrTime = DateTime.Now.ToString("HH:mm:ss");
                    Thread.Sleep(1000);
                }
            });
        }
        private void OnPlayListItemTap(object obj)
        {
            if (SiderViewVisible)
            {
                StartTimer();
            }
            var info = obj as ChannelInfo;
            if (info == null) return;
            //先暂停
            GlobalService.GetAction("OnPause")();
            try
            {
                MediaSource = MediaSource.FromUri(info.Url.FirstOrDefault());
            }
            catch (Exception ex)
            {
                Toast.Make(ex.Message, CommunityToolkit.Maui.Core.ToastDuration.Short).Show();

            }
            Title = info.Title;
            SelectItem(info);

            if (!SiderViewVisible)
            {
                InfoViewVisible = true;
                StartTimer();
            }
        }
        void SelectItem(ChannelInfo info)
        {
            foreach (var item in PlayList)
            {
                if (item.Title == info.Title)
                {
                    item.Selected = true;
                    //info.Selected = true;
                }
                else
                {
                    item.Selected = false;
                }
            }
        }
        public string GetNextSource()
        {
            if (PlayList.Count <= 0)
            {
                return string.Empty;
            }
            var selectedInfo = GetSelectedChannelInfo();
            int i = -1;
            if (MediaSource is UriMediaSource)
            {
                var source = MediaSource as UriMediaSource;
                i = selectedInfo.Url.IndexOf(source.Uri.ToString());
            }
            else
            {
                return string.Empty;
            }
            if (i + 1 >= selectedInfo.Url.Count || i < 0) return string.Empty;
            return selectedInfo.Url[i + 1];
        }
        async void LoadPlayList(bool autoPlay = true)
        {
            await Task.Delay(10);
            ChannelInfo selectedInfo = null;
            if (this.PlayList.Count > 0)
            {
                selectedInfo = GetSelectedChannelInfo();
            }
            string file = Path.Combine(FileSystem.AppDataDirectory, "live.txt");

            try
            {
                byte[] result = await new HttpClient().GetByteArrayAsync("http://jaylosy.com.cn/live.txt");
                Debug.Write($"写入文件{file}");
                File.WriteAllText(file, Encoding.UTF8.GetString(result));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            if (!File.Exists(file)) return;
            var lines = File.ReadAllLines(file);
            var lineList = lines.ToList();
            lineList.RemoveAll(a => !a.Contains(",http") && !a.Contains(",rtmp"));
            lastUpdateDate = DateTime.Now;
            if (lineList.Count <= 0) return;
            PlayList.Clear();
            lineList.Select(a => new
            {
                Title = a.Split(",")[0],
                Url = a.Split(",")[1]
            }).ToList().ForEach(a =>
            {
                var tmp = PlayList.Where(b => b.Title == a.Title).FirstOrDefault();
                bool hasFind = tmp != null;
                if (tmp == null)
                {
                    tmp = new ChannelInfo
                    {
                        Title = a.Title,
                        Url = new ObservableCollection<string> { a.Url }
                    };
                }
                else
                {
                    tmp.Url.Add(a.Url);
                }
                if (selectedInfo != null)
                {
                    if (selectedInfo.Title == tmp.Title)
                    {
                        tmp.Selected = true;
                        selectedInfo = tmp;
                    }
                }
                else
                {
                    tmp.Selected = true;
                    selectedInfo = tmp;
                }
                if (!hasFind)
                {
                    PlayList.Add(tmp);
                }
            });
            if (autoPlay) OnPlayListItemTap(selectedInfo);
        }
        private void OnMediaTap()
        {
            SiderViewVisible = !SiderViewVisible;
            if (SiderViewVisible)
            {
                StartTimer();
            }
        }
        private void OnMediaSwipeUp(object obj)
        {
            MediaSwipeUp(obj, true);
        }
        public void MediaSwipeUp(object obj, bool autoPlay)
        {
            if (PlayList.Count <= 0) return;
            var listView = obj as MauiListView;
            var selectedInfo = GetSelectedChannelInfo();
            int index = PlayList.IndexOf(selectedInfo);
            int count = PlayList.Count;
            if (index == count - 1)
            {
                index = 0;
            }
            else
            {
                index++;
            }
            if (autoPlay)
                OnPlayListItemTap(PlayList[index]);
            else
                SelectItem(PlayList[index]);
            try
            {
                listView.SelectItem(index);
            }
            catch (Exception ex)
            {
                Toast.Make(ex.Message, CommunityToolkit.Maui.Core.ToastDuration.Short).Show();
            }
        }
        private void OnMediaSwipeDown(object obj)
        {
            MediaSwipeDown(obj, true);
        }

        public void MediaSwipeDown(object obj, bool autoPlay)
        {
            if (PlayList.Count <= 0) return;
            var listView = obj as MauiListView;
            var selectedInfo = GetSelectedChannelInfo();
            int index = PlayList.IndexOf(selectedInfo);
            int count = PlayList.Count;
            if (index == 0)
            {
                index = count - 1;
            }
            else
            {
                index--;
            }
            if (autoPlay)
                OnPlayListItemTap(PlayList[index]);
            else
                SelectItem(PlayList[index]);
            try
            {
                listView.SelectItem(index);
            }
            catch (Exception ex)
            {
                Toast.Make(ex.Message, CommunityToolkit.Maui.Core.ToastDuration.Short).Show();
            }
        }

        public ChannelInfo GetSelectedChannelInfo()
        {
            return PlayList.Where(a => a.Selected).FirstOrDefault();
        }
    }
}
