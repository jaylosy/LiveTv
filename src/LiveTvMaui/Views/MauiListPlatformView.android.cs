﻿using Android.Content;
using Android.Media;
using Android.Views;
using Android.Views.TextClassifiers;
using Android.Widget;
using LiveTvMaui.Models;
using LiveTvMaui.ViewHandles;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AndroidX.RecyclerView.Widget.RecyclerView;
using ListView = Android.Widget.ListView;
namespace LiveTvMaui.Views
{
    public class MauiListViewItem:Java.Lang.Object
    {
        public string Title { get; set; }

        public List<string> Urls { get; set; }

        public bool IsSelected { get; set; }
    }
    public class MauiListViewAdapter : BaseAdapter
    {
        private  List<MauiListViewItem> itemList ;
        private readonly Context context;

        public override int Count => itemList.Count;
        public MauiListViewAdapter(Context context, List<MauiListViewItem> itemList)
        {
            this.context = context;
            this.itemList = itemList;
        }
        public void SetItems(List<MauiListViewItem> itemList)
        {
            this.itemList = itemList;
        }
        public void SelectItem(int position)
        {
            for(int i=0;i<itemList.Count;i++)
            {
                if (i == position)
                {
                    itemList[i].IsSelected = true;
                }
                else
                {
                    itemList[i].IsSelected = false;
                }
            }
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return itemList[position];
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Android.Views.View GetView(int position, Android.Views.View convertView, ViewGroup parent)
        {
            var data = itemList[position];
            var view = convertView;
            if (view == null)
            {
                view = LayoutInflater.From(context).Inflate(Resource.Layout.maui_list_item, null);
            }
            var tv1 = view.FindViewById<TextView>(Resource.Id.textView1);
            var tv2 = view.FindViewById<TextView>(Resource.Id.textView2);
            tv1.Text = data.Title;
            tv2.Text = data.Title;
            if (data.IsSelected)
            {
                tv1.Visibility = ViewStates.Invisible;
                tv2.Visibility = ViewStates.Visible;
            }
            else
            {
                tv1.Visibility = ViewStates.Visible;
                tv2.Visibility = ViewStates.Invisible;
            }
            return view;
        }
    }
    public class MauiListPlatformView : ListView
    {
        private readonly MauiListViewHandle viewHandle;
        private IEnumerable<object> items;

        public MauiListPlatformView(MauiListViewHandle viewHandle) : base(Platform.CurrentActivity)
        {
            this.viewHandle = viewHandle;
            this.SmoothScrollbarEnabled = true;
            this.Scroll += MauiListPlatformView_Scroll;
            this.ItemClick += MauiListPlatformView_ItemClick;
        }

        private void MauiListPlatformView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var item = items.ToList()[e.Position];
            viewHandle.VirtualView.OnItemSelect(item as ChannelInfo);
            SetSelection(e.Position);
        }

        private void MauiListPlatformView_Scroll(object sender, ScrollEventArgs e)
        {
            viewHandle.VirtualView.OnScroll();
        }

        public override void SetSelection(int position)
        {
            
            base.SetSelection(position);
            var adapter = (MauiListViewAdapter)Adapter;
            adapter.SelectItem(position);
            adapter.NotifyDataSetChanged();
            this.SmoothScrollToPosition(position);
        }
        
        public void SetItems(IEnumerable<object> items)
        {
            this.items = items;
            if (items == null)
            {
                items = Enumerable.Empty<object>();
            }
            var list = items.Select(a => new MauiListViewItem
            {
                Title = ((ChannelInfo)a).Title,
                Urls = ((ChannelInfo)a).Url.ToList(),
                IsSelected = ((ChannelInfo)a).Selected
            }).ToList();
            if (Adapter == null)
            {
                Adapter = new MauiListViewAdapter(Context, list);
                var adapter = (MauiListViewAdapter)Adapter;
                adapter.NotifyDataSetChanged();
            }
            else
            {
                var adapter = (MauiListViewAdapter)Adapter;
                adapter.SetItems(list);
                adapter.NotifyDataSetChanged();
            }
            

        }



    }
}
