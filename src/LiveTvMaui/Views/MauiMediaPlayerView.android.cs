﻿using Android.Content;
using Android.Media;
using Android.Widget;
using Com.Google.Android.Exoplayer2;
using LiveTvMaui.Controls;
using LiveTvMaui.MediaPlugin;
using LiveTvMaui.ViewHandles;
using Microsoft.Maui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Android.Graphics.ImageDecoder;
using static Android.Widget.GridLayout;

namespace LiveTvMaui.Views
{
    public class MauiMediaPlayerView : RelativeLayout
    {
        BaseMediaInterface _mediaInterface;
        static Context context = Platform.CurrentActivity;
        private Aspect _aspect;
        private bool _playWhenReady;
        private string _source;
        private float _volume;
        private MediaKernel _mediaKernel;
        private Controls.MediaCodec _mediaCodec;
        private readonly MauiMediaPlayerViewHandle handle;

        public MauiMediaPlayerView(MauiMediaPlayerViewHandle handle) : base(context)
        {
            this.handle = handle;
        }
        internal void SetKernel(MediaKernel mediaKernel)
        {
            _mediaKernel = mediaKernel;
            RemoveAllViews();
            if (_mediaInterface != null)
            {
                _mediaInterface.OnRelease();
            }
            switch (mediaKernel)
            {
                case MediaKernel.谷歌播放器:
                    _mediaInterface = new ExoPlayerMediaInterface(handle);
                    break;
                case MediaKernel.阿里播放器:
                    _mediaInterface = new AliPlayerMediaInterface(handle);
                    break;
                case MediaKernel.内置播放器:
                    _mediaInterface = new DefaultPlayerMediaInterface(handle);
                    break;
            }
            _mediaInterface.LayoutParameters = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent);
            AddView(_mediaInterface);

            if (!string.IsNullOrEmpty(_source))
            {
                SetAutoPlay(_playWhenReady);
                SetAspect(_aspect);
                SetVolume(_volume);
                SetUrl(_source);
            }
        }

        internal void SetCodec(Controls.MediaCodec mediaCodec)
        {
            _mediaCodec = mediaCodec;
            _mediaInterface.SetMediaCodec((int)mediaCodec);

        }
        internal void SetAspect(Aspect aspect)
        {
            _aspect = aspect;
            _mediaInterface.SetAspect(aspect);
        }

        internal void SetAutoPlay(bool playWhenReady)
        {
            _playWhenReady = playWhenReady;
            _mediaInterface.SetAutoPlay(playWhenReady);
        }

        internal void SetUrl(string source)
        {
            _source = source;
            _mediaInterface.SetUrl(source);
        }

        internal void SetVolume(float volume)
        {
            _volume = volume;
            _mediaInterface.SetVolume(volume);
        }

        internal void RequestStop()
        {
            _mediaInterface.OnStop();
        }

        internal void RequestPause()
        {
            _mediaInterface.OnPause();
        }

        internal void RequestPlay()
        {
            _mediaInterface.OnPlay();
        }

    }
}
