﻿using LiveTvMaui.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grid = Microsoft.UI.Xaml.Controls.Grid;
namespace LiveTvMaui.Views
{
    public class MauiMediaPlayerView : Grid, IDisposable
    {
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        internal void RequestPause()
        {
            throw new NotImplementedException();
        }

        internal void RequestPlay()
        {
            throw new NotImplementedException();
        }

        internal void RequestStop()
        {
            throw new NotImplementedException();
        }

        internal void SetAspect(Aspect aspect)
        {
            throw new NotImplementedException();
        }

        internal void SetAutoPlay(bool playWhenReady)
        {
            throw new NotImplementedException();
        }

        internal void SetCodec(MediaCodec codec)
        {
            throw new NotImplementedException();
        }

        internal void SetKernel(MediaKernel kernel)
        {
            throw new NotImplementedException();
        }

        internal void SetUrl(string source)
        {
            throw new NotImplementedException();
        }

        internal void SetVolume(float volume)
        {
            throw new NotImplementedException();
        }
    }
}
